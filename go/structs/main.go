package main

import (
	"fmt"
)

type contactInfo struct {
  email string
  zipCode int
}

type person struct {
  firstName string
  lastName string
  contactInfo  //#contactInfo contactInfo
}

func main() {
  jim := person{
    firstName: "Jim",
    lastName: "Afdf",
    contactInfo: contactInfo {
      email: "jim@gmail.com",
      zipCode: 94000,
    },
  }

  jim.updateName("Jimmy")
  jim.print()
}

func (p *person) updateName(newFirstName string) {
  p.firstName = newFirstName
}

func (p person) print() {
  fmt.Printf("%+v", p)
}