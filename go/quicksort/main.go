package main

import (
	"bufio"
	"fmt"
	"os"
	"strings"
	"strconv"
)

func quicksort(a []int) []int {
	if len(a) < 2 { return a }
	left, right := 0, len(a) - 1

	p := 0

	a[p], a[right] = a[right], a[p]

	for i := range a {
		if a[i] < a[right] {
			a[i], a[left] = a[left], a[i]
			left++
		}
	}

	a[left], a[right] = a[right], a[left]

	quicksort(a[:left])
	quicksort(a[left+1:])
	return a
}

func main() {
	var values []int
	reader := bufio.NewReader(os.Stdin)
	fmt.Print("Enter text: ")
	text, _ := reader.ReadString('\n')
	words := strings.Fields(text)
	for _, word := range words {
		convertedInt, _ := strconv.Atoi(word)
		values = append(values, convertedInt)
	}
	fmt.Printf("Read ints: %v\n", values)

	sorted := quicksort(values)
	fmt.Printf("Sorted: %v", sorted)
}